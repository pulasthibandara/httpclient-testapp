import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {EmployeeDetailComponent} from'./employee-detail/employee-detail.component';
import {EmployeeListComponent} from'./employee-list/employee-list.component';
import {EmployeeService} from './employee.service';
import { from } from 'rxjs';

@NgModule({
  declarations: [
    AppComponent,
    EmployeeDetailComponent,
    EmployeeListComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [EmployeeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
